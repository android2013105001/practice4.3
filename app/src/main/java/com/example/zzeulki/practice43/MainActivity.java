package com.example.zzeulki.practice43;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Onclick(View v)
    {
        int id = v.getId();
        Intent i;

        switch(id)
        {
            case R.id.call :
                i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:0539408695"));
                startActivity(i);
                break;
            case R.id.contact :
                i = new Intent(Intent.ACTION_VIEW, Uri.parse("content://contacts/people"));
                startActivity(i);
                break;
            case R.id.map :
                i = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:37.58,126.98"));
                startActivity(i);
                break;
            case R.id.web :
                i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com/"));
                startActivity(i);
                break;
        }
    }
}
